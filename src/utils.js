const vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'h'];
const Utils = {
  generateRandomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  startsWithVowel(word) {
    return word && vowels.includes(word.charAt(0).toLowerCase());
  },

  shouldAddSpace(elementToCheck) {
    return elementToCheck.charAt(elementToCheck.length - 1) !== '\'';
  },

  mergePrepWithArticle(prep, obj) {
    const combos = [
      {
        prep: 'à',
        masculine: 'au',
        feminine: 'à la',
        plural: 'aux',
        special: 'à l\''
      },
      {
        prep: 'de',
        masculine: 'du',
        feminine: 'de la',
        plural: 'des',
        special: 'de l\''
      }
    ];
    let filtered = combos.find(c => c.prep === prep);

    if (obj.singular) {
      if (vowels.includes(obj.name.charAt(0).toLowerCase())) {
        return filtered.special;
      } else {
        return filtered[obj.gender]
      }
    } else {
      return filtered.plural;
    }
  }
}

export default Utils;
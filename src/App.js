import React from 'react';
import { Link as RouterLink, Route, withRouter, Switch } from "react-router-dom";
import logo from './logo.svg';
import './App.scss';
import { AppBar, Typography, Tabs, Tab } from '@material-ui/core';
import Intro from './js/components/Intro';
import DirectIndirectContainer from './js/containers/DirectIndirect/DirectIndirectContainer';
import NotFound from './js/components/NotFound';
import YContainer from './js/containers/Y/YContainer';
import EnContainer from './js/containers/En/EnContainer';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0,
      topBarPosition: 'sticky'
    };

    this.handleRouteToPageWithSubBar = this.handleRouteToPageWithSubBar.bind(this);
  }

  /*
    Set active tab on page load.
    Set up listener to set the active tab when routing. Removes the need for handling change on click of tabs.
  */
  componentWillMount() {
    const { history } = this.props;

    this.setActiveTab(this.calculateActiveTab(history.location.pathname));

    this.unlisten = history.listen((location) => {
      this.setActiveTab(this.calculateActiveTab(location.pathname));
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  /*
    When routing to a component that contains another AppBar, set the top level one to be static instead of sticky.
  */
  handleRouteToPageWithSubBar(hasSubBar) {
    this.setState({ topBarPosition: hasSubBar ? 'static' : 'sticky' });
  }

  /*
    Based on a given path, return the index of a tab in the top nav to later activate it.
  */
  calculateActiveTab(path) {
    let tabValue;

    switch(path.split('/')[1]) {
      case '':
        tabValue = 0;
        break;
      case 'direct-indirect':
        tabValue = 1;
        break;
      case 'en':
        tabValue = 2;
        break;
      case 'y':
        tabValue = 3;
        break;
      default:
        tabValue = 0;
        break;
    }

    return tabValue;
  }

  /*
    Set a given tab to active based on an index.
  */
  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  render() {
    const { tabValue } = this.state;

    return (
      <div className="App">
        <AppBar position={this.state.topBarPosition} id="top-app-bar" className="app-bar">
          <div>
            <Typography variant="h6" component="p" color="inherit" align="left">
              FRENCH PRONOUN TRAINER
            </Typography>
            <Tabs value={tabValue} id="top-nav-tabs">
              <Tab label={<div><p>Start here</p></div>} component={RouterLink} to="/" />
              <Tab label={<div><p>Le La Les Lui Leur</p><small>As Direct &amp; Indirect Object Pronouns</small></div>} component={RouterLink} to="/direct-indirect" />
              <Tab label={<div><p>En</p><small>As a Direct &amp; Indirect Object Pronoun</small></div>} component={RouterLink} to="/en" />
              <Tab label={<div><p>Y</p><small>As an Indirect Object Pronoun</small></div>} component={RouterLink} to="/y" />
            </Tabs>
          </div>
        </AppBar>
        <Switch>
          <Route exact path="/" render={(props) => <Intro {...props} onRouteLoad={this.handleRouteToPageWithSubBar} />} />
          <Route path="/direct-indirect" render={(props) => <DirectIndirectContainer {...props} onRouteLoad={this.handleRouteToPageWithSubBar} />} />
          <Route path="/en" render={(props) => <EnContainer {...props} onRouteLoad={this.handleRouteToPageWithSubBar} />} />
          <Route path="/y" render={(props) => <YContainer {...props} onRouteLoad={this.handleRouteToPageWithSubBar} />} />
          <Route path="/*" component={NotFound} />
          <Route component={Intro} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(App);

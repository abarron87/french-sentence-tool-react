import React from 'react';

/*
  Component that renders tooltip content as HTML.
*/
const TooltipText = (props) => {
  const { heading, subheading } = props.content;
  return (
    <aside className="tooltip-content">
      <h1>{heading}</h1>
      <h2>{subheading}</h2>
    </aside>
  );
}

export default TooltipText;
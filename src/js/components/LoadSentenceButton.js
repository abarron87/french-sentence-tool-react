import React from 'react';
import Button from '@material-ui/core/Button';

/*
  Component to render a button that will generate the next stage of the process to breakdown a sentence.
*/
class LoadSentenceButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.levelToLoad);
  }

  render() {
    return <Button variant="contained" color="primary" onClick={this.handleClick}>{this.props.buttonText}</Button>;
  }
}

export default LoadSentenceButton;
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Tooltip } from '@material-ui/core';

/*
  Component that renders the subject element of a sentence.
*/
const Subject = (props) => {
  const { classes, tooltip } = props;

  return (
    <Typography inline={true} variant="inherit" component="span" className={classes.container} onMouseEnter={props.onMouseEnter} onMouseLeave={props.onMouseLeave}>
      <Tooltip title={tooltip} enterDelay={250}>
        <Typography inline={true} variant="inherit" component="span" className={'subject with-tooltip ' + classes.element + ' ' + classes.subject.join(' ')}>{props.subject}</Typography>
      </Tooltip>
    </Typography>
  );
}

export default Subject;
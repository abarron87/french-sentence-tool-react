import React from 'react';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Component that renders the indirect object element of a sentence.
*/
const IndirectObject = (props) => {
  const { classes } = props;
  const { tooltip } = props;

  return (
    <Typography inline={true} variant="inherit" component="span"className={classes.container}>
      <Tooltip title={tooltip} enterDelay={250}>
        <Typography inline={true} variant="inherit" component="span" className={'indirect-object with-tooltip ' + classes.element + ' ' + classes.indirectObj.join(' ')}>{props.text}</Typography>
      </Tooltip>
    </Typography>
  );
}

export default IndirectObject;
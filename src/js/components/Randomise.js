import React from 'react';
import { IconButton, Tooltip } from '@material-ui/core';
import RefreshRoundedIcon from '@material-ui/icons/RefreshRounded';

/*
  Randomise is a presentational component responsible for rendering a button which when clicked will randomly generate a new sentence for the given verb.
*/
const Randomise = (props) => {
  return (
    <IconButton aria-label="Get new random sentence." color="secondary" onClick={props.onClick} disabled={props.disabled}>
      <Tooltip title="Click to get a new sentence with this verb.">
        <RefreshRoundedIcon />
      </Tooltip>
    </IconButton>
  );
}

export default Randomise;
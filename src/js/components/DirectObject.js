import React from 'react';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Component that renders the direct element of a sentence.
*/
const DirectObject = (props) => {
  const { classes } = props;
  const { tooltip } = props;

  return (
    <Typography inline={true} variant="inherit" component="span" className={classes.container}>
      <Tooltip title={tooltip} enterDelay={250}>
        <Typography inline={true} variant="inherit" component="span" className={'direct-object with-tooltip ' + classes.element + ' ' + classes.directObj.join(' ')}>{props.text}</Typography>
      </Tooltip>
    </Typography>
  );
}

export default DirectObject;
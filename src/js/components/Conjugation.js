import React from 'react';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Component that renders the conjugation element of a sentence.
*/
const Conjugation = (props) => {
  const { classes, tooltip } = props;

  return (
    <Typography inline={true} component="span" variant="inherit" className={classes.container}>
      <Tooltip title={tooltip} enterDelay={250}>
        <Typography inline={true} variant="inherit" component="span" className={'conjugation with-tooltip ' + classes.element + ' ' + classes.conjugation.join(' ')}>{props.conjugation}</Typography>
      </Tooltip>
    </Typography>
  );
}

export default Conjugation;

import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

/*
  Component responsible for rendering a drop down from which to select a verb.
*/
class VerbSelection extends React.Component {
  render() {
    const { selectedVerb, onChange, list } = this.props;
    let verbList = list.map(verb => {
      return <MenuItem key={verb.name} value={verb.name}>{verb.prettyName}</MenuItem>;
    });

    return (
      <form>
        <FormControl id="verb-selection-container">
          <InputLabel htmlFor="verbs">Verbs</InputLabel>
          <Select
            value={selectedVerb}
            onChange={onChange}
            inputProps={{
              name: 'selected-verb',
              id: 'selected-verb',
            }}
          >
            <MenuItem value=""></MenuItem>
            {verbList}
          </Select>
        </FormControl>
      </form>
    );
  }
}

export default VerbSelection;
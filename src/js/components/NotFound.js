import React, { Fragment } from 'react';
import { Link as RouterLink } from "react-router-dom";
import { Typography } from '@material-ui/core';

/*
  Component to be rendered when an unknown route is hit.
*/
const NotFound = () => {
  return (
    <Fragment>
      <Typography variant="h1">404. Page not found.</Typography>
      <Typography variant="h2">The page you are looking for has not been found. <RouterLink to="/">Return home.</RouterLink></Typography>
    </Fragment>
  );
}

export default NotFound;
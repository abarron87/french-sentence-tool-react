import React, { Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Typography, Tooltip, ExpansionPanelSummary, ExpansionPanelDetails, ExpansionPanel } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

/*
  Component to render the content for the start page.
*/
class Intro extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      orderDiagramExpanded: true
    }

    this.handleExpansionPanelChange = this.handleExpansionPanelChange.bind(this);
  }

  componentWillMount() {
    this.props.onRouteLoad(false);
  }

  handleExpansionPanelChange() {
    this.setState({
      orderDiagramExpanded: !this.state.orderDiagramExpanded
    });
  }

  render() {
    const { orderDiagramExpanded } = this.state;

    return (
      <div className="intro-component">
        <Typography variant="h1">Come to practice French sentence structure?</Typography>
        <Typography variant="h1"><Tooltip title="That's informal French for great!" className="with-tooltip"><Typography variant="inherit">Nickel!</Typography></Tooltip></Typography>
        <div className="container">
          <Typography variant="h4">You may have seen this diagram before:</Typography>
          <ExpansionPanel expanded={orderDiagramExpanded} id="order-diagram-container" onChange={this.handleExpansionPanelChange}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography>Sentence Element Order Diagram</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div id="order-diagram">
                <div>
                  <Typography variant="h5" component="div">Subject</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">(ne)</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">me</Typography>
                  <Typography variant="h5" component="div">te</Typography>
                  <Typography variant="h5" component="div">se</Typography>
                  <Typography variant="h5" component="div">nous</Typography>
                  <Typography variant="h5" component="div">vous</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">le</Typography>
                  <Typography variant="h5" component="div">la</Typography>
                  <Typography variant="h5" component="div">l'</Typography>
                  <Typography variant="h5" component="div">les</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">lui</Typography>
                  <Typography variant="h5" component="div">leur</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">y</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">en</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">verb</Typography>
                </div>
                <div>
                  <Typography variant="h5" component="div">(pas)</Typography>
                </div>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel> 
          <Typography variant="h4" className="after-expansion-panel">It's a start but you're probably still scratching your head/stroking your beard.</Typography>
          <Typography variant="h4">Our tools let you play around with <em>real sentences</em> in order to have you creating complex sentences with confidence. <RouterLink to="/direct-indirect">Click here!</RouterLink></Typography>
        </div>
      </div>
    );
  }
}

export default Intro;
import React, { Fragment } from 'react';
import { Typography } from '@material-ui/core';
import Subject from '../../components/Subject';
import Conjugation from '../../components/Conjugation';
import DirectObject from '../../components/DirectObject';
import IndirectObject from '../../components/IndirectObject';
import Utils from '../../../utils';
import TooltipTextContainer from '../TooltipTextContainer';

const classes = {
  sentence: 'sentence',
  container: 'sentence-element-container',
  element: 'sentence-element'
};

/*
  Container component for the Direct Indirect section of the app. Repeated code will at some point be refactored into Higher Order Components.
*/
class SentenceContainer extends React.Component {
  constructor(props) {
    super(props);

    this.handleMouseEnterOrLeave = this.handleMouseEnterOrLeave.bind(this);
  }

  get subject() {
    let compareProp;
    const { level, verb } = this.props;

    switch(level) {
      case 1:
        compareProp = verb.name;
        break;
      case 2:
        compareProp = null;
        break;
      case 3:
        compareProp = null;
        break;
      default:
        compareProp = verb.name;
        break;
    }

    return (compareProp && Utils.startsWithVowel(compareProp)) ? 'J\'' : 'Je';
  }

  shouldContractCODPronoun(obj, nextWord) {
    return obj.singular && Utils.startsWithVowel(nextWord);
  }

  buildDirectObjectData(obj, usePronoun, nextEl) {
    let contents = {};
    let shouldContract = this.shouldContractCODPronoun(obj, nextEl);

    if (usePronoun) {
      // if the next word is the verb's conjugation, we contract the pronoun if the word starts with a vowel.
      if (shouldContract) {
        contents.text = 'l\'';
        contents.shouldContract = true;
      } else {
        contents.text = obj.pronoun;
        contents.shouldContract = false;
      }
    } else {
      contents.text = obj.article;
  
      // if article is NOT "l'" then add a space.
      if (Utils.shouldAddSpace(contents.text)) {
        contents.text += ' ';
      }
  
      contents.text += obj.name;
    }

    return contents;
  }

  buildIndirectObjectData(obj, prep) {
    let contents = { prep: prep };
    let prepPlusArticle;
    // If the object is a person's name, it won't have an article. Just print the preposition.
    if (!obj.namedPerson) {
      prepPlusArticle = Utils.mergePrepWithArticle(prep, obj);
      contents.text = prepPlusArticle;
      contents.finalPrep = prepPlusArticle;
    } else {
      contents.text = prep;
      contents.finalPrep = prep;
    }

    // if the contents so far are NOT "de l'" or "à l'" then add a space.
    if (Utils.shouldAddSpace(contents.text)) {
      contents.text += ' ';
    }

    contents.text += obj.name;

    return contents;
  }

  handleMouseEnterOrLeave(e) {
    const elementRegex = /(subject|conjugation|direct-object|indirect-object)/;
    const foundName = e.target.className.match(elementRegex);
    const foundLevel = e.target.closest('.sentence').className.match(/level-(\d)/);
    
    (foundName && foundLevel) && this.props.onMouseEnterOrLeave(foundName[1], foundLevel[1]);
    
    this.toggleHoverState(e.target);
  }

  toggleHoverState(element) {
    const hoverClass = 'hovered';

    if(Array.from(element.classList).includes(hoverClass)) {
      element.classList.remove(hoverClass);
    } else {
      element.classList.add(hoverClass);
    }
  }

  get sentenceContents() {
    let contents;
    const { verb, level, highlight, hoveredLevel, hasDirectObject, hasIndirectObject } = this.props;
    let { directObj, indirectObj } = this.props.sentenceElements;
    let directObjData;
    let indirectObjData;
    let hasIndTransType = verb.types.find(type => type.name === 'indirect-transitive');
    let elementClasses = Object.assign({}, classes);
    let elementMap = {
      'subject': 'subject',
      'conjugation': 'conjugation',
      'direct-object': 'direct-object',
      'indirect-object': 'indirect-object'
    }

    if(level === 1) {

      if (hasDirectObject) {
        directObjData = Object.assign({}, directObj, this.buildDirectObjectData(directObj, false));
        elementClasses.directObj = ['full-object'];
      }

      elementClasses.subject = [];
      elementClasses.conjugation = [];
      elementClasses.indirectObj = ['full-object'];

      if(hasIndirectObject) {
        indirectObjData = Object.assign({}, indirectObj, this.buildIndirectObjectData(indirectObj, hasIndTransType.preps[0]));
      } else {
        elementClasses.directObj.push('last-element');
      }

      if(Utils.shouldAddSpace(this.subject)) {
        elementClasses.subject.push('expanded-pronoun');
      }

      if(highlight && hoveredLevel !== 1) {
        elementClasses[elementMap[highlight]].push('highlighted');
      }

      contents = (
        <Typography variant="h2" component="p" className={`${classes.sentence} level-1`}>
          <Subject subject={this.subject} classes={elementClasses} tooltip={<TooltipTextContainer elementName="subject" />} onMouseEnter={this.handleMouseEnterOrLeave} onMouseLeave={this.handleMouseEnterOrLeave} />
          <Conjugation conjugation={verb.conjugation} classes={elementClasses} tooltip={<TooltipTextContainer elementName="conjugation" />} />
          {hasDirectObject && <DirectObject text={directObjData.text} classes={elementClasses} tooltip={<TooltipTextContainer elementName="directObj" sentenceLevel={1} />} />}
          {hasIndirectObject && <IndirectObject text={indirectObjData.text} classes={elementClasses} tooltip={<TooltipTextContainer elementName="indirectObj" sentenceLevel={1} obj={indirectObjData} />} />}  
        </Typography>
      );
    } else if(level === 2) {

      if (hasDirectObject) {
        directObjData = Object.assign({}, directObj, this.buildDirectObjectData(directObj, true, verb.conjugation));
        elementClasses.directObj = ['cod-pronoun'];

        if(directObjData.shouldContract) {
          elementClasses.directObj.push('contracted-pronoun');
        } else {
          elementClasses.directObj.push('expanded-pronoun');
        }
      }

      elementClasses.subject = ['expanded-pronoun'];
      elementClasses.conjugation = [];
      elementClasses.indirectObj = [];

      if(hasIndirectObject) {
        if (!hasDirectObject) {
          elementClasses.conjugation.push('last-element');
          elementClasses.indirectObj.push('coi-pronoun', 'coi-pronoun-1');
        } else {
          elementClasses.indirectObj.push('full-object');
        }

        indirectObjData = Object.assign({}, indirectObj, this.buildIndirectObjectData(indirectObj, hasIndTransType.preps[0]));
      } else {
        elementClasses.conjugation.push('last-element');
      }

      contents = {
        subject: <Subject subject={this.subject} shouldAddSpace={true} classes={elementClasses} tooltip={<TooltipTextContainer elementName="subject" />} onMouseEnter={this.handleMouseEnterOrLeave} onMouseLeave={this.handleMouseEnterOrLeave} />,
        conjugation: <Conjugation conjugation={verb.conjugation} classes={elementClasses} tooltip={<TooltipTextContainer elementName="conjugation" />} />,
      }

      if (hasDirectObject) {
        contents.directObj = <DirectObject text={directObjData.text} classes={elementClasses} tooltip={<TooltipTextContainer elementName="directObj" sentenceLevel={2} obj={directObjData} />} />;
      }
      
      if (hasIndirectObject) {
        if (!hasDirectObject) {
          // if we're at level 2 and there's no direct object, there MUST be an indirect object.
          contents.indirectObj = <IndirectObject text={indirectObjData.pronoun} classes={elementClasses} tooltip={<TooltipTextContainer elementName="indirectObj" sentenceLevel={2} obj={indirectObjData} isPronoun={true} />} />;
        } else {
          contents.indirectObj = <IndirectObject text={indirectObjData.text} classes={elementClasses} tooltip={<TooltipTextContainer elementName="indirectObj" sentenceLevel={2} obj={indirectObjData} />} />;
        }
      }

      contents = (
        <Typography variant="h2" component="p" className={`${classes.sentence} level-2`}>
          {contents.subject}
          {hasDirectObject ? contents.directObj : contents.indirectObj}
          {contents.conjugation}
          {hasDirectObject && contents.indirectObj}
        </Typography>
      );
    } else if(level === 3) {
      directObjData = Object.assign({}, directObj, this.buildDirectObjectData(directObj, true, indirectObj.pronoun));
      indirectObjData = Object.assign({}, indirectObj, this.buildIndirectObjectData(indirectObj, hasIndTransType.preps[0]));

      elementClasses.subject = ['expanded-pronoun'];
      elementClasses.conjugation = ['last-element'];
      elementClasses.directObj = ['cod-pronoun'];
      elementClasses.indirectObj = ['coi-pronoun', 'coi-pronoun-1'];

      // including this, but the direct pronoun will always be followed by lui or leur (no vowel) so it will never be true for now.
      if(directObjData.shouldContract) {
        elementClasses.directObj.push('contracted-pronoun');
      } else {
        elementClasses.directObj.push('expanded-pronoun');
      }

      contents = (
        <Typography variant="h2" component="p" className={`${classes.sentence} level-3`}>
          <Subject subject={this.subject} classes={elementClasses} tooltip={<TooltipTextContainer elementName="subject" />} onMouseEnter={this.handleMouseEnterOrLeave} onMouseLeave={this.handleMouseEnterOrLeave} />
          <DirectObject text={directObjData.text} classes={elementClasses} tooltip={<TooltipTextContainer elementName="directObj" sentenceLevel={3} obj={directObjData} />} />
          <IndirectObject classes={elementClasses} text={indirectObjData.pronoun}  tooltip={<TooltipTextContainer elementName="indirectObj" sentenceLevel={3} obj={indirectObjData} />} />
          <Conjugation conjugation={verb.conjugation} classes={elementClasses} tooltip={<TooltipTextContainer elementName="conjugation" />} />
        </Typography>
      );
    }

    return contents;
  }

  render() {
    return this.sentenceContents;
  }
}

export default SentenceContainer;
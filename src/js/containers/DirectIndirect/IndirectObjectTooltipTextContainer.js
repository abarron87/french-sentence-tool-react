import React from 'react';
import TooltipText from '../../components/TooltipText';
import tooltips from '../../data/tooltips';

/*
  IndirectObjectTooltipTextContainer
  Responsible for constructing the tooltip text for Indirect Objects, in various forms.
*/
class IndirectObjectTooltipTextContainer extends React.Component {
  render() {
    const { obj, sentenceLevel, isPronoun } = this.props;
    const tooltipObj = tooltips.indirectObj.find(tt => (tt.levels.includes(sentenceLevel) && tt.forPronoun == !!isPronoun));
    let dynamicText;
    let finalObj = { heading: tooltipObj.heading };

    if (sentenceLevel === 1 || (sentenceLevel === 2 && !isPronoun)) {
      if (!obj.article) {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.noArticle === true).text.supplant({ prep: obj.prep });
      } else {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.noArticle === true).text.supplant({ prep: obj.prep, article: obj.article, combined: obj.finalPrep });
      }

      finalObj.subheading = `${tooltipObj.subheadingsOptions.baseText} ${dynamicText}`;
    } else {
      finalObj.subheading = tooltipObj.subheading.supplant({ pronoun: obj.pronoun, singularOrPlural: (obj.singular ? 'singular' : 'plural'), prep: obj.prep });
    }

    return <TooltipText content={finalObj} />;
  }
}

export default IndirectObjectTooltipTextContainer;
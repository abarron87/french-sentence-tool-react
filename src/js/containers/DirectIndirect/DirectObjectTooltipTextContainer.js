import React, { Fragment } from 'react';
import TooltipText from '../../components/TooltipText';
import tooltips from '../../data/tooltips';

/*
  DirectObjectTooltipTextContainer
  Responsible for constructing the tooltip text for Direct Objects, in various forms.
*/
class DirectObjectTooltipTextContainer extends React.Component {
  render() {
    const { obj, sentenceLevel } = this.props;
    const tooltipObj = tooltips.directObj.find(tt => tt.levels.includes(sentenceLevel));

    if (sentenceLevel === 1) {
      return <TooltipText content={tooltipObj} />
    } else {
      let finalObj = { heading: tooltipObj.heading };
      let dynamicText;

      // MAYBE REWRITING SUPPLANT TO USE A DIFFERENT CHARACTER WILL HELP ADD MARKUP IN TOOLTIPS.

      if (obj.shouldContract) {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.contracted).text.supplant({ pronoun: obj.pronoun });
      } else if (obj.pronoun === 'en') {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.pronoun === 'en').text.supplant({ pronoun: obj.pronoun, object: `${obj.article} ${obj.name}`, article: obj.article });
      } else if (!obj.singular) {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.plural).text.supplant({ pronoun: obj.pronoun, object: `${obj.article} ${obj.name}` });
      } else {
        dynamicText = tooltipObj.subheadingsOptions.options.find(o => o.contracted === false).text.supplant({ pronoun: obj.pronoun, object: `${obj.article} ${obj.name}`, gender: obj.gender });
      }

      finalObj.subheading = <Fragment>{tooltipObj.subheadingsOptions.baseText} {dynamicText}</Fragment>;

      return <TooltipText content={finalObj} />
    }
  }
}

export default DirectObjectTooltipTextContainer;
import React, { Fragment } from 'react';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { Link as RouterLink, Switch, Route, Redirect } from 'react-router-dom';
import DirectContainer from './DirectContainer';
import IndirectContainer from './IndirectContainer';
import BothObjectsContainer from './BothObjectsContainer';

/*
  Component for the Direct Indirect section.
*/
class DirectIndirectContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0
    };

    this.handleNavChange = this.handleNavChange.bind(this);
  }

  componentWillMount() {
    const { history } = this.props;

    this.setActiveTab(this.calculateActiveTab(history.location.pathname));
    this.props.onRouteLoad(true);

    this.unlisten = history.listen((location) => {
      this.setActiveTab(this.calculateActiveTab(location.pathname));
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  calculateActiveTab(path) {
    let tabValue;

    switch(path.split('/').pop()) {
      case 'direct':
        tabValue = 0;
        break;
      case 'indirect':
        tabValue = 1;
        break;
      case 'both':
        tabValue = 2;
        break;
      default:
        tabValue = 0;
        break;
    }

    return tabValue;
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  handleNavChange(e, value) {
    this.setState({
      tabValue: value
    });
  }

  render() {
    const { tabValue } = this.state;
    const { match } = this.props;

    return (
      <Fragment>
        <AppBar color="secondary" position="sticky">
          <Tabs value={tabValue} onChange={this.handleNavChange}>
            <Tab label="Verbs with direct objects" component={RouterLink} to="/direct-indirect/direct" />
            <Tab label="Verbs with indirect objects" component={RouterLink} to="/direct-indirect/indirect" />
            <Tab label="Verbs with both" component={RouterLink} to="/direct-indirect/both" />
          </Tabs>
        </AppBar>
        <div className="container">
          <Switch>
            <Redirect exact from={`${match.url}`} to={`${match.url}/direct`} />
            <Route path={`${match.url}/direct`} component={DirectContainer} />
            <Route path={`${match.url}/indirect`} component={IndirectContainer} />
            <Route path={`${match.url}/both`} component={BothObjectsContainer} />
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default DirectIndirectContainer;
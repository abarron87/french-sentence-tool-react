import React, { Fragment } from 'react';

import VerbSelection from '../../components/VerbSelection';
import Randomise from '../../components/Randomise';
import SentenceContainer from './SentenceContainer';
import LoadSentenceButton from '../../components/LoadSentenceButton';
import verbList from '../../data/verbs';
import Utils from '../../../utils';
import sentenceData from '../../data/sentenceData';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Component for the Direct object sub-section of the Direct Indirect section.
*/
class DirectContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = ({
      currentVerb: '',
      sentencesLoaded: 0,
      sentenceElements: {},
      highlightedElement: null,
      hoveredLevel: 0,
      tabValue: 0
    });

    this.handleVerbChange = this.handleVerbChange.bind(this);
    this.handleRandomiseButtonClick = this.handleRandomiseButtonClick.bind(this);
    this.handleLoadSentenceButtonClick = this.handleLoadSentenceButtonClick.bind(this);
    this.handleSentenceElementMouseEnterOrLeave = this.handleSentenceElementMouseEnterOrLeave.bind(this);
    this.handleNavChange = this.handleNavChange.bind(this);
    this.currentRandInt = -1;
  }

  /*
    Always generate a new set of sentence elements different
    to last set.
  */
  getNewSentenceElements(verbName) {
    let haystack = sentenceData.filter(obj => obj.worksWith.includes(verbName));
    let randInt = this.currentRandInt; // this way it will always go round the loop at least once.

    while(randInt === this.currentRandInt) {
      randInt = Utils.generateRandomInteger(0, (haystack.length - 1));
    }

    // once out of the loop, update the currentRandInt property for the starting point next time.
    this.currentRandInt = randInt;

    return haystack[this.currentRandInt];
  }

  handleVerbChange(e) {
    this.setState({ 
      currentVerb: e.target.value,
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(e.target.value)
    });
  }

  handleRandomiseButtonClick() {
    this.setState({
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(this.state.currentVerb)
    });
  }

  handleSentenceElementMouseEnterOrLeave(sentenceElementName, level) {
    // figure out a way to send a highlight state or classname to the sentences who don't match the passed level.
    // might need to set it on state here so that re-render will be caused.
    console.log('handleSentenceElementMouseEnterOrLeave', arguments);
    this.setState({ highlightedElement: sentenceElementName, hoveredLevel: level });
  }

  handleSentenceUpdate() {
    this.setState({ sentencesLoaded: this.state.sentencesLoaded + 1 });
  }

  handleLoadSentenceButtonClick(level) {
    this.setState({ sentencesLoaded: level });
  }

  handleNavChange(e, value) {
    this.setActiveTab(value);
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  render() {
    let firstSentenceContainer;
    let secondSentenceContainer;
    let { currentVerb, tabValue } = this.state;
    let verbObj = verbList.find(v => v.name === this.state.currentVerb);

    if(this.state.currentVerb && this.state.sentenceElements) {
      firstSentenceContainer = <SentenceContainer verb={verbObj} level={1} hasDirectObject={true} sentenceElements={this.state.sentenceElements} highlight={this.state.highlightedElement} hoveredLevel={this.state.hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;
      
      if(this.state.sentencesLoaded === 2) {
        secondSentenceContainer = <SentenceContainer verb={verbObj} level={2} hasDirectObject={true} sentenceElements={this.state.sentenceElements} hoveredLevel={this.state.hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;
      }
    }

    return (
      <Fragment>
        {!currentVerb && <Typography variant="h3">Some verbs take a <Tooltip title="En français: un complément d'objet direct or COD."><span className="with-tooltip">direct object</span></Tooltip> with a <strong>definite</strong> article (le, la, les, l'). <br /> These are replaced by a pronoun that looks like the article.</Typography>}
        {!currentVerb && <Typography variant="h4">Choose one below to play around with the word order.</Typography>}
        <div className="flex-row">
          <VerbSelection list={verbList.filter(v => (!!(v.types.filter(t => t.name === 'direct-transitive')).length) && !(v.types.filter(t => t.name === 'indirect-transitive')).length)} onChange={this.handleVerbChange} selectedVerb={currentVerb} />
          {currentVerb && <Randomise onClick={this.handleRandomiseButtonClick} disabled={!!(sentenceData.filter(obj => obj.worksWith.includes(currentVerb)).length === 1)} />}
        </div>

        {firstSentenceContainer}
        { (this.state.sentencesLoaded === 1) && <LoadSentenceButton onClick={this.handleLoadSentenceButtonClick} levelToLoad={2} buttonText="Replace direct object with pronoun" /> }
        {secondSentenceContainer}
      </Fragment>
    );
  }
}

export default DirectContainer;
import React from 'react';
import TooltipText from '../components/TooltipText';
import tooltips from '../data/tooltips';
import DirectObjectTooltipTextContainer from './DirectIndirect/DirectObjectTooltipTextContainer';
import IndirectObjectTooltipTextContainer from './DirectIndirect/IndirectObjectTooltipTextContainer';

/*
  Component to render the correct container component based on the parent sentence element requiring a tooltip.
*/
class TooltipTextContainer extends React.Component {
  render() {
    const { elementName, sentenceLevel, obj } = this.props;

    if(elementName === 'subject' || elementName === 'conjugation') {
      return <TooltipText content={tooltips[elementName]} />;
    } else if(elementName === 'directObj') {
      return <DirectObjectTooltipTextContainer obj={obj} sentenceLevel={sentenceLevel} />
    } else if(elementName === 'indirectObj') {
      return <IndirectObjectTooltipTextContainer obj={obj} sentenceLevel={sentenceLevel} {...this.props} />
    }
  }
}

export default TooltipTextContainer;
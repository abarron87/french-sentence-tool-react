import React, { Fragment } from 'react';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { Link as RouterLink, Switch, Route, Redirect } from 'react-router-dom';
import EnDirectContainer from './EnDirectContainer';
import EnIndirectContainer from './EnIndirectContainer';

/*
  Component for the EN section.
*/
class EnContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0
    };

    this.handleNavChange = this.handleNavChange.bind(this);
  }

  componentWillMount() {
    const { history } = this.props;

    this.setActiveTab(this.calculateActiveTab(history.location.pathname));
    this.props.onRouteLoad(true);

    this.unlisten = history.listen((location) => {
      this.setActiveTab(this.calculateActiveTab(location.pathname));
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  calculateActiveTab(path) {
    let tabValue;

    switch(path.split('/').pop()) {
      case 'direct':
        tabValue = 0;
        break;
      case 'indirect':
        tabValue = 1;
        break;
      case 'both':
        tabValue = 2;
        break;
      default:
        tabValue = 0;
        break;
    }

    return tabValue;
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  handleNavChange(e, value) {
    this.setState({
      tabValue: value
    });
  }

  render() {
    const { tabValue } = this.state;
    const { match } = this.props;

    return (
      <Fragment>
        <AppBar color="secondary" position="sticky">
          <Tabs value={tabValue} onChange={this.handleNavChange}>
            <Tab label="Verbs with direct objects" component={RouterLink} to={`${match.url}/direct`}/>
            <Tab label="Verbs with indirect objects" component={RouterLink} to={`${match.url}/indirect`} />
            {/* <Tab label="Verbs with both" component={RouterLink} to={`${match.url}/both`} /> */}
          </Tabs>
        </AppBar>
        <div className="container">
          <Switch>
            <Redirect exact from={`${match.url}`} to={`${match.url}/direct`} />
            <Route path={`${match.url}/direct`} component={EnDirectContainer} />
            <Route path={`${match.url}/indirect`} component={EnIndirectContainer} />
            {/* <Route path={`${match.url}/both`} component={EnBothObjectsContainer} /> */}
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default EnContainer;
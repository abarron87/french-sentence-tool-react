import React, { Fragment } from 'react';

import VerbSelection from '../../components/VerbSelection';
import Randomise from '../../components/Randomise';
import EnSentenceContainer from './EnSentenceContainer';
import LoadSentenceButton from '../../components/LoadSentenceButton';
import verbListEn from '../../data/verbListEn';
import Utils from '../../../utils';
import sentenceDataEn from '../../data/sentenceDataEn';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Component for the Direct object sub-section of the EN section.
*/
class EnDirectContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = ({
      currentVerb: '',
      sentencesLoaded: 0,
      sentenceElements: {},
      highlightedElement: null,
      hoveredLevel: 0,
      tabValue: 0
    });

    this.handleVerbChange = this.handleVerbChange.bind(this);
    this.handleRandomiseButtonClick = this.handleRandomiseButtonClick.bind(this);
    this.handleLoadSentenceButtonClick = this.handleLoadSentenceButtonClick.bind(this);
    this.handleSentenceElementMouseEnterOrLeave = this.handleSentenceElementMouseEnterOrLeave.bind(this);
    this.handleNavChange = this.handleNavChange.bind(this);
    this.currentRandInt = -1;
  }

  /*
    Always generate a new set of sentence elements different
    to last set.
  */
  getNewSentenceElements(verbName) {
    let haystack = sentenceDataEn.filter(obj => obj.worksWith.includes(verbName));
    let randInt = this.currentRandInt; // this way it will always go round the loop at least once.

    while(randInt === this.currentRandInt) {
      randInt = Utils.generateRandomInteger(0, (haystack.length - 1));
    }

    // once out of the loop, update the currentRandInt property for the starting point next time.
    this.currentRandInt = randInt;

    return haystack[this.currentRandInt];
  }

  handleVerbChange(e) {
    this.setState({ 
      currentVerb: e.target.value,
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(e.target.value)
    });
  }

  handleRandomiseButtonClick() {
    this.setState({
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(this.state.currentVerb)
    });
  }

  handleSentenceElementMouseEnterOrLeave(sentenceElementName, level) {
    // figure out a way to send a highlight state or classname to the sentences who don't match the passed level.
    // might need to set it on state here so that re-render will be caused.
    console.log('handleSentenceElementMouseEnterOrLeave', arguments);
    this.setState({ highlightedElement: sentenceElementName, hoveredLevel: level });
  }

  handleSentenceUpdate() {
    this.setState({ sentencesLoaded: this.state.sentencesLoaded + 1 });
  }

  handleLoadSentenceButtonClick(level) {
    this.setState({ sentencesLoaded: level });
  }

  handleNavChange(e, value) {
    this.setActiveTab(value);
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  render() {
    let firstSentenceContainer;
    let secondSentenceContainer;
    let { currentVerb, sentencesLoaded, sentenceElements, highlightedElement, hoveredLevel } = this.state;
    let verbObj = verbListEn.find(v => v.name === currentVerb);

    if(currentVerb && sentenceElements) {
      firstSentenceContainer = <EnSentenceContainer verb={verbObj} level={1} hasDirectObject={true} sentenceElements={sentenceElements} highlight={highlightedElement} hoveredLevel={hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;
      
      if(sentencesLoaded === 2) {
        secondSentenceContainer = <EnSentenceContainer verb={verbObj} level={2} hasDirectObject={true} sentenceElements={sentenceElements} hoveredLevel={hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;
      }
    }

    return (
      <Fragment>
        {!currentVerb && <Typography variant="h3">Some verbs take a <Tooltip title="En français: un complément d'objet direct or COD."><span className="with-tooltip">direct object</span></Tooltip> with an indefinite article (du, de la, des). <br />These objects are replaced by <strong>en</strong>.</Typography>}
        {!currentVerb && <Typography variant="h4">Choose one below to play around with the word order.</Typography>}
        <div className="flex-row">
          <VerbSelection list={verbListEn.filter(v => (!!(v.types.filter(t => t.name === 'direct-transitive')).length) && !(v.types.filter(t => t.name === 'indirect-transitive')).length)} onChange={this.handleVerbChange} selectedVerb={currentVerb} />
          {currentVerb && <Randomise onClick={this.handleRandomiseButtonClick} disabled={!!(sentenceDataEn.filter(obj => obj.worksWith.includes(currentVerb)).length === 1)} />}
        </div>

        {firstSentenceContainer}
        { (sentencesLoaded === 1) && <LoadSentenceButton onClick={this.handleLoadSentenceButtonClick} levelToLoad={2} buttonText="Replace direct object with 'en'" /> }
        {secondSentenceContainer}
      </Fragment>
    );
  }
}

export default EnDirectContainer;
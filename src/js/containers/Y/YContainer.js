import React, { Fragment } from 'react';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { Link as RouterLink, Switch, Route, Redirect } from 'react-router-dom';
import YIndirectContainer from './YIndirectContainer';

/*
  Component for the Y section.
*/
class YContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabValue: 0
    };

    this.handleNavChange = this.handleNavChange.bind(this);
  }

  componentWillMount() {
    const { history } = this.props;

    this.setActiveTab(this.calculateActiveTab(history.location.pathname));
    this.props.onRouteLoad(true);

    this.unlisten = history.listen((location) => {
      this.setActiveTab(this.calculateActiveTab(location.pathname));
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  calculateActiveTab(path) {
    let tabValue;

    switch(path.split('/').pop()) {
      case 'indirect':
        tabValue = 0;
        break;
      default:
        tabValue = 0;
        break;
    }

    return tabValue;
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  handleNavChange(e, value) {
    this.setState({
      tabValue: value
    });
  }

  render() {
    const { tabValue } = this.state;
    const { match } = this.props;

    return (
      <Fragment>
        <AppBar color="secondary" position="sticky">
          <Tabs value={tabValue} onChange={this.handleNavChange}>
            <Tab label="Verbs with indirect objects" component={RouterLink} to={`${match.url}/indirect`} />
          </Tabs>
        </AppBar>
        <div className="container">
          <Switch>
            <Redirect exact from={`${match.url}`} to={`${match.url}/indirect`} />
            <Route path={`${match.url}/indirect`} component={YIndirectContainer} />
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default YContainer;
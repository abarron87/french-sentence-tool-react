import React, { Fragment } from 'react';

import VerbSelection from '../../components/VerbSelection';
import Randomise from '../../components/Randomise';
import YSentenceContainer from './YSentenceContainer';
import LoadSentenceButton from '../../components/LoadSentenceButton';
import verbListY from '../../data/verbListY';
import Utils from '../../../utils';
import sentenceDataY from '../../data/sentenceDataY';
import { Typography, Tooltip } from '@material-ui/core';

/*
  Container component for the Y section.
*/
class YIndirectContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = ({
      currentVerb: '',
      sentencesLoaded: 0,
      sentenceElements: {},
      highlightedElement: null,
      hoveredLevel: 0,
      tabValue: 0
    });

    this.handleVerbChange = this.handleVerbChange.bind(this);
    this.handleRandomiseButtonClick = this.handleRandomiseButtonClick.bind(this);
    this.handleLoadSentenceButtonClick = this.handleLoadSentenceButtonClick.bind(this);
    this.handleSentenceElementMouseEnterOrLeave = this.handleSentenceElementMouseEnterOrLeave.bind(this);
    this.handleNavChange = this.handleNavChange.bind(this);
    this.currentRandInt = -1;
  }

  /*
    Always generate a new set of sentence elements different
    to last set.
  */
  getNewSentenceElements(verbName) {
    let haystack = sentenceDataY.filter(obj => obj.worksWith.includes(verbName));
    let randInt = this.currentRandInt; // this way it will always go round the loop at least once.

    while(randInt === this.currentRandInt) {
      randInt = Utils.generateRandomInteger(0, (haystack.length - 1));
    }

    // once out of the loop, update the currentRandInt property for the starting point next time.
    this.currentRandInt = randInt;

    return haystack[this.currentRandInt];
  }

  handleVerbChange(e) {
    this.setState({ 
      currentVerb: e.target.value,
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(e.target.value)
    });
  }

  handleRandomiseButtonClick() {
    this.setState({
      sentencesLoaded: 1,
      sentenceElements: this.getNewSentenceElements(this.state.currentVerb)
    });
  }

  handleSentenceElementMouseEnterOrLeave(sentenceElementName, level) {
    // figure out a way to send a highlight state or classname to the sentences who don't match the passed level.
    // might need to set it on state here so that re-render will be caused.
    this.setState({ highlightedElement: sentenceElementName, hoveredLevel: level });
  }

  handleSentenceUpdate() {
    this.setState({ sentencesLoaded: this.state.sentencesLoaded + 1 });
  }

  handleLoadSentenceButtonClick(level) {
    this.setState({ sentencesLoaded: level });
  }

  handleNavChange(e, value) {
    this.setActiveTab(value);
  }

  setActiveTab(tabValue) {
    this.setState({ tabValue: tabValue });
  }

  render() {
    let firstSentenceContainer;
    let secondSentenceContainer;
    let { currentVerb, sentencesLoaded, sentenceElements, highlightedElement, hoveredLevel } = this.state;
    let verbObj = verbListY.find(v => v.name === currentVerb);
    let buttonText = '';

    if (currentVerb && sentenceElements) {
      firstSentenceContainer = <YSentenceContainer verb={verbObj} level={1} hasIndirectObject={true} sentenceElements={sentenceElements} highlight={highlightedElement} hoveredLevel={hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;

      if (sentencesLoaded === 1) {
        buttonText = 'Replace indirect object with \'y\'';
      } else if (sentencesLoaded === 2) {
        secondSentenceContainer = <YSentenceContainer verb={verbObj} level={2} hasIndirectObject={true} sentenceElements={sentenceElements} hoveredLevel={hoveredLevel} onMouseEnterOrLeave={this.handleSentenceElementMouseEnterOrLeave} />;
      } 
    }

    return (
      <Fragment>
        {!currentVerb && <Typography variant="h3">Some verbs take an <Tooltip title="En français: un complément d'objet indirect or COI."><span className="with-tooltip">indirect object</span></Tooltip> introduced by the preposition <strong>à</strong>. <br />These are replaced by <strong>y</strong> when <em>not</em> a person.</Typography>}
        {!currentVerb && <Typography variant="h4">Choose one below to play around with the word order.</Typography>}
        <div className="flex-row">
          <VerbSelection list={verbListY.filter(v => (!(v.types.filter(t => t.name === 'direct-transitive')).length) && !!(v.types.filter(t => t.name === 'indirect-transitive')).length)} onChange={this.handleVerbChange} selectedVerb={currentVerb} />
          {currentVerb && <Randomise onClick={this.handleRandomiseButtonClick} disabled={!!(sentenceDataY.filter(obj => obj.worksWith.includes(currentVerb)).length === 1)} />}
        </div>

        {firstSentenceContainer}
        { (sentencesLoaded === 1) && <LoadSentenceButton onClick={this.handleLoadSentenceButtonClick} levelToLoad={2} buttonText={buttonText} /> }
        {secondSentenceContainer}
      </Fragment>
    );
  }
}

export default YIndirectContainer;
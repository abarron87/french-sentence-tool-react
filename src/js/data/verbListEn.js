const verbListEn = [
  {
    name: 'donner',
    prettyName: 'Donner',
    conjugation: 'donne',
    types: [
      {
        name: 'direct-transitive'
      },
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'à',
            objectTypes: ['people']
          }
        ]
      }
    ]
  },
  {
    name: 'parler',
    prettyName: 'Parler',
    conjugation: 'parle',
    types: [
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'de',
            objectTypes: ['objects']
          }
        ]
      }
    ]
  },
  {
    name: 'avoir',
    prettyName: 'Avoir',
    conjugation: 'ai',
    types: [
      {
        name: 'direct-transitive'
      }
    ]
  },
  {
    name: 'prendre',
    prettyName: 'Prendre',
    conjugation: 'prends',
    types: [
      {
        name: 'direct-transitive',
        preps: [
          {
            name: 'à',
            objectTypes: ['objects']
          }
        ]
      }
    ]
  },
  {
    name: 'dependre',
    prettyName: 'Dépendre',
    conjugation: 'dépend',
    types: [
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'de',
            objectTypes: ['objects']
          }
        ]
      }
    ]
  },
]

export default verbListEn;
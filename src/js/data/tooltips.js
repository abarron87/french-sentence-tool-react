import React, { Fragment } from 'react';

/*
  Tooltip text for the various sentence elements.
*/
const tooltips = {
  subject: {
    heading: 'The subject.',
    subheading: 'The one who performs the action.'
  },
  conjugation: {
    heading: 'The conjugation.',
    subheading: 'The form of main verb; unique to the subject, and the verb tense.'
  },
  directObj: [
    {
      levels: [1],
      heading: 'A direct object.',
      subheading: <Fragment>These receive the action of the verb. They are <strong>never</strong> introduced by a pronoun.</Fragment>
    },
    {
      levels: [2, 3],
      heading: 'A direct object pronoun (COD).',
      subheadingsOptions: {
        baseText: 'The direct object has been replaced by a pronoun.',
        options: [
          {
            gender: 'masculine|feminine',
            contracted: false,
            text: 'Here it\'s "{pronoun}" because "{object}" is {gender}.'
          },
          {
            plural: true,
            text: 'Here it\'s "{pronoun}" because "{object}" is a plural noun.'
          },
          {
            contracted: true,
            gender: 'masculine|feminine',
            text: 'Here it\'s "{pronoun}" because the following letter is a vowel.'
          },
          {
            pronoun: 'en',
            gender: 'masculine|feminine',
            text: 'Here it\'s "{pronoun}" because "{object}" has an indefinite article: "{article}".'
          }
        ]
      }
    }
  ],
  indirectObj: [
    {
      levels: [1, 2],
      forPronoun: false,
      heading: 'An indirect object.',
      subheadingsOptions: {
        baseText: 'Always introduced by a preposition (that\'s how you recognise them). In this case:',
        options: [
          {
            noArticle: true,
            text: '{prep}.'
          },
          {
            noArticle: false,
            text: '"{prep} + {article} = {combined}".'
          }
        ]
      }
    },
    {
      levels: [2, 3],
      forPronoun: true,
      heading: 'An indirect object pronoun (COI).',
      subheading: '"{pronoun}" represents the {singularOrPlural} indirect object from the previous sentence. It is "{pronoun}" because the preposition that introduces the indirect object is "{prep}".'
    }
  ]
}

export default tooltips;
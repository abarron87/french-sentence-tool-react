// List of verbs for the Y section.
const verbListY = [
  {
    name: 'penser',
    prettyName: 'Penser',
    conjugation: 'pense',
    types: [
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'à',
            objectTypes: ['people', 'objects']
          }
        ]
      }
    ]
  },
  {
    name: 'repondre',
    prettyName: 'Répondre',
    conjugation: 'réponds',
    types: [
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'à',
            objectTypes: ['objects']
          }
        ]
      }
    ]
  },
  {
    name: 'reflechir',
    prettyName: 'Réflechir',
    conjugation: 'réfléchis',
    types: [
      {
        name: 'indirect-transitive',
        preps: [
          {
            name: 'à',
            objectTypes: ['people', 'objects']
          }
        ]
      }
    ]
  }
]

export default verbListY;
// Verb data for the DirectIndirect section.
const verbList = [
  {
    name: 'aimer',
    prettyName: 'Aimer',
    conjugation: 'aime',
    types: [{ name: 'direct-transitive' }]
  },
  {
    name: 'acheter',
    prettyName: 'Acheter',
    conjugation: 'achète',
    types: [
      { name: 'direct-transitive' },
      { name: 'indirect-transitive', 
          preps: ['à']
      }
    ]
  },
  {
    name: 'donner',
    prettyName: 'Donner',
    conjugation: 'donne',
    types: [
      {
        name: 'direct-transitive'
      },
      {
        name: 'indirect-transitive',
        preps: ['à']
      }
    ]
  },
  {
    name: 'avoir',
    prettyName: 'Avoir',
    conjugation: 'ai',
    types: [{ name: 'direct-transitive' }]
  },
  {
    name: 'manger',
    prettyName: 'Manger',
    conjugation: 'mange',
    types: [{ name: 'direct-transitive' }]
  },
  {
    name: 'envoyer',
    prettyName: 'Envoyer',
    conjugation: 'envoie',
    types: [{ name: 'direct-transitive'}, { name: 'indirect-transitive', preps: ['à'] }]
  },
  {
    name: 'mentir',
    prettyName: 'Mentir',
    conjugation: 'mens',
    types: [{ name: 'indirect-transitive', preps: ['à'] }, { name: 'intransitive' }]
  },
  {
    name: 'repondre',
    prettyName: 'Répondre',
    conjugation: 'réponds',
    types: [{ name: 'indirect-transitive', preps: ['à'] }, { name: 'intransitive' }]
  }
];

export default verbList;
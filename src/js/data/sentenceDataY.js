// Sentence data for the Y section.
const sentenceDataY = [
  {
    directObj: { article: 'des', name: 'pommes', gender: 'feminine', singular: false },
    indirectObj: { name: 'Alex', gender: 'masculine', singular: true, pronoun: 'lui', namedPerson: true },
    worksWith: ['donner', 'manger', 'envoyer', 'avoir', 'prendre']
  },
  {
    // directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'la', name: 'crise bancaire', gender: 'feminine', singular: true },
    worksWith: ['penser', 'reflechir']
  },
  {
    // directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'l\'', name: 'offre d\'emploi', gender: 'feminine', singular: true },
    worksWith: ['penser', 'reflechir', 'repondre']
  },
  {
    // directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'les', name: 'cocktails au rhum', gender: 'masculine', singular: false },
    worksWith: ['penser', 'reflechir']
  },
  {
    // directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'le', name: 'rendez-vous à la préfecture', gender: 'masculine', singular: true },
    worksWith: ['penser', 'reflechir']
  },
  {
    // directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'les', name: 'questions', gender: 'feminine', singular: false },
    worksWith: ['repondre', 'penser', 'reflechir']
  }
]

export default sentenceDataY;
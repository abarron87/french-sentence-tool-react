// Sentence data for the Direct Indirect section.
const sentenceData = [
  {
    directObj: { article: 'la', name: 'pomme', gender: 'feminine', singular: true, pronoun: 'la' },
    indirectObj: { name: 'Alex', gender: 'masculine', singular: true, pronoun: 'lui', namedPerson: true },
    worksWith: ['donner', 'aimer', 'manger', 'envoyer']
  },
  {
    directObj: { article: 'le', name: 'document', gender: 'masculine', singular: true, pronoun: 'le' },
    indirectObj: { article: 'le', name: 'patron', gender: 'masculine', singular: true, pronoun: 'lui' },
    worksWith: ['donner', 'aimer', 'envoyer']
  },
  {
    directObj: { article: 'l\'', name: 'animal', gender: 'masculine', singular: true, pronoun: 'le' },
    indirectObj: { article: 'les', name: 'propriétaires', gender: 'masculine', singular: false, pronoun: 'leur' },
    worksWith: ['donner', 'aimer', 'manger', 'envoyer']
  },
  {
    directObj: { article: 'les', name: 'enfants', gender: 'masculine', singular: false, pronoun: 'les' },
    indirectObj: { article: 'les', name: 'parents', gender: 'masculine', singular: false, pronoun: 'leur' },
    worksWith: ['donner', 'aimer']
  },
  {
    indirectObj: { article: 'les', name: 'spectateurs', gender: 'masculine', singular: false, pronoun: 'leur' },
    worksWith: ['mentir', 'repondre']
  },
  {
    indirectObj: { article: 'le', name: 'prisonnier', gender: 'masculine', singular: true, pronoun: 'lui' },
    worksWith: ['mentir', 'repondre']
  },
  {
    indirectObj: { article: 'la', name: 'boulangère', gender: 'feminine', singular: true, pronoun: 'lui' },
    worksWith: ['mentir', 'repondre']
  },
  {
    indirectObj: { article: 'l\'', name: 'architecte', gender: 'masculine', singular: true, pronoun: 'lui' },
    worksWith: ['mentir', 'repondre']
  },
  {
    directObj: { article: 'les', name: 'enfants', gender: 'masculine', singular: false, pronoun: 'les' },
    indirectObj: { article: 'les', name: 'parents', gender: 'masculine', singular: false, pronoun: 'leur' },
    worksWith: ['donner', 'aimer']
  },
  // aimed at 'penser'
  {
    directObj: { article: 'la', name: 'femme', gender: 'feminine', singular: true, pronoun: 'la', adjectives: ['folle', 'contente', 'heureuse', 'maline', 'intelligente', 'belle'] },
    indirectObj: { article: 'la', name: 'conversation d\'hier', gender: 'feminine', singular: true, pronoun: 'y' },
    worksWith: ['penser', 'aimer']
  }
]

export default sentenceData;
const sentenceDataEn = [
  {
    directObj: { article: 'des', name: 'pommes', gender: 'feminine', singular: false },
    indirectObj: { name: 'Alex', gender: 'masculine', singular: true, pronoun: 'lui', namedPerson: true },
    worksWith: ['donner', 'manger', 'envoyer', 'avoir', 'prendre']
  },
  {
    directObj: { article: 'des', name: 'conseils', gender: 'masculine', singular: false },
    indirectObj: { article: 'le', name: 'patron', gender: 'masculine', singular: true, pronoun: 'lui' },
    worksWith: ['donner', 'envoyer', 'parler']
  },
  {
    directObj: { article: 'du', name: 'fromage d\'hier', gender: 'masculine', singular: true },
    indirectObj: { article: 'du', name: 'fromage d\'hier', gender: 'masculine', singular: true },
    worksWith: ['parler']
  },
  {
    directObj: { article: 'de la', name: 'crise bancaire', gender: 'feminine', singular: true },
    indirectObj: { article: 'la', name: 'crise bancaire', gender: 'feminine', singular: true },
    worksWith: ['parler']
  },
  {
    directObj: { article: 'de la', name: 'crise bancaire', gender: 'feminine', singular: true },
    indirectObj: { article: 'l\'', name: 'argent', gender: 'masculine', singular: true },
    worksWith: ['parler', 'dependre']
  },
  {
    directObj: { article: 'de la', name: 'crise bancaire', gender: 'feminine', singular: true },
    indirectObj: { article: 'le', name: 'boulot', gender: 'masculine', singular: true },
    worksWith: ['parler']
  }
  // {
  //   directObj: { article: 'l\'', name: 'animal', gender: 'masculine', singular: true, pronoun: 'le' },
  //   indirectObj: { article: 'les', name: 'propriétaires', gender: 'masculine', singular: false, pronoun: 'leur' },
  //   worksWith: ['donner', 'manger', 'envoyer']
  // },
  // {
  //   directObj: { article: 'les', name: 'enfants', gender: 'masculine', singular: false, pronoun: 'les' },
  //   indirectObj: { article: 'les', name: 'parents', gender: 'masculine', singular: false, pronoun: 'leur' },
  //   worksWith: ['donner']
  // },
  // {
  //   indirectObj: { article: 'les', name: 'spectateurs', gender: 'masculine', singular: false, pronoun: 'leur' },
  //   worksWith: ['mentir', 'répondre']
  // },
  // {
  //   indirectObj: { article: 'le', name: 'prisonnier', gender: 'masculine', singular: true, pronoun: 'lui' },
  //   worksWith: ['mentir', 'répondre']
  // },
  // {
  //   indirectObj: { article: 'la', name: 'boulangère', gender: 'feminine', singular: true, pronoun: 'lui' },
  //   worksWith: ['mentir', 'répondre']
  // },
  // {
  //   indirectObj: { article: 'l\'', name: 'architecte', gender: 'masculine', singular: true, pronoun: 'lui' },
  //   worksWith: ['mentir', 'répondre']
  // },
  // {
  //   directObj: { article: 'les', name: 'enfants', gender: 'masculine', singular: false, pronoun: 'les' },
  //   indirectObj: { article: 'les', name: 'parents', gender: 'masculine', singular: false, pronoun: 'leur' },
  //   worksWith: ['donner']
  // }
]

export default sentenceDataEn;